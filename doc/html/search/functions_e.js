var searchData=
[
  ['save_5fpad',['save_pad',['../root__utils_8h.html#ae646dc89429b10d6b87bc0af026e88ee',1,'save_pad(TPad *pad, const char *saveName, int x=700, int y=500):&#160;root_utils.cxx'],['../root__utils_8h.html#a03035db434c0958029381a0b3db50bcf',1,'save_pad(TVirtualPad *pad, const char *saveName, int x=700, int y=500):&#160;root_utils.cxx']]],
  ['scale_5fbins',['scale_bins',['../root__utils_8h.html#a1d05a5a4865a00b66c9b222ac2cb6786',1,'root_utils.cxx']]],
  ['set_5fbin_5ferr_5ffrom_5fdiff',['set_bin_err_from_diff',['../root__utils_8h.html#a8cacf8babb173e3ea6cee6b87ed7724e',1,'set_bin_err_from_diff(TGraphAsymmErrors *grOrig, TGraph *grVar0, TGraph *grVar1):&#160;root_utils.cxx'],['../root__utils_8h.html#a609d475f8fcb2c1e7dac07d4272d858e',1,'set_bin_err_from_diff(TH1F *hOrig, TH1F *hVar0, TH1F *hVar1, bool norm=false):&#160;root_utils.cxx']]],
  ['set_5fclose_5fbins_5fto_5fone',['set_close_bins_to_one',['../root__utils_8h.html#a24a55d63491e537beccdacd7837463d8',1,'root_utils.cxx']]],
  ['set_5fgeo_5ffile',['set_geo_file',['../classgeo__plot.html#a0eed61dd1bc53d5613f6f450dee93479',1,'geo_plot']]],
  ['set_5finfo',['set_info',['../classgeo__plot.html#acf0de1dbec086d45827eb9cec72fa78c',1,'geo_plot::set_info(bool b)'],['../classgeo__plot.html#a99c67d9d45197a5ca1cffacc939c6fdf',1,'geo_plot::set_info(std::string s)']]],
  ['set_5flow_5fleft_5finfo',['set_low_left_info',['../classgeo__plot.html#adcbadb0804d8eb6ea8f6e3a5242cac70',1,'geo_plot']]],
  ['set_5flow_5fright_5finfo',['set_low_right_info',['../classgeo__plot.html#a656f0d132dfa55fb802e80ce4d7c8247',1,'geo_plot']]],
  ['set_5fmax_5fof_5fhists',['set_max_of_hists',['../root__utils_8h.html#ace2a6c0b9497483bf99eca7623464960',1,'root_utils.cxx']]],
  ['set_5fno_5faxis',['set_no_axis',['../classgeo__plot.html#a323632ee28dac52737be68dbc4e3f65c',1,'geo_plot']]],
  ['set_5foutput_5ffile',['set_output_file',['../classgeo__plot.html#a2a45dc1828bb1ba90f0cd44be8cc1503',1,'geo_plot']]],
  ['set_5fpad_5fnumber_5foption',['set_pad_number_option',['../classgeo__plot.html#a2f13ddce57385857a093e5d1cc69b88c',1,'geo_plot']]],
  ['set_5fpad_5fscale',['set_pad_scale',['../classgeo__plot.html#a891efd4c69e1ef4e10e38d6f2c5ea66c',1,'geo_plot']]],
  ['set_5fpad_5ftype_5fscale',['set_pad_type_scale',['../classgeo__plot.html#aaf1d44a66509011cb3c81a0daa94dadf',1,'geo_plot']]],
  ['set_5fpad_5ftype_5fstrings',['set_pad_type_strings',['../classflat__plot.html#aa0b8a736e022d0d745f0d260eeb2479d',1,'flat_plot']]],
  ['set_5fpad_5fvalue_5fprecision',['set_pad_value_precision',['../classgeo__plot.html#a68148d6c7f34e32ad70f9ad5b215d060',1,'geo_plot']]],
  ['set_5ftop_5fleft_5finfo',['set_top_left_info',['../classgeo__plot.html#a77e078847525a7f697b36edb6fe718ba',1,'geo_plot']]],
  ['set_5ftop_5fright_5finfo',['set_top_right_info',['../classgeo__plot.html#ac35db4b5216b2698d46edf9f6036cba8',1,'geo_plot']]],
  ['set_5fverbose',['set_verbose',['../classgeo__plot.html#a30af0cc56e2e3e22b7dd817d4702b0c3',1,'geo_plot']]],
  ['set_5fz_5frange',['set_z_range',['../classgeo__plot.html#a6acdf52a70db3be907a5c4392fdceac1',1,'geo_plot']]],
  ['set_5fz_5ftitle',['set_z_title',['../classgeo__plot.html#af7449dfaea36940d069afb4123efdc93',1,'geo_plot']]],
  ['set_5fzero_5fsuppress',['set_zero_suppress',['../classgeo__plot.html#af11e8927d7ef77b851f6af23f1c65fdd',1,'geo_plot']]],
  ['shear_5fpolygon',['shear_polygon',['../root__utils_8h.html#a90ef4ca5a8f687b28b1cdd9e6804fe3c',1,'root_utils.cxx']]],
  ['shift_5fpalette_5faxis',['shift_palette_axis',['../root__utils_8h.html#ad67e62c730642972e92f40296f7821f5',1,'root_utils.cxx']]],
  ['shift_5fx_5fvalues',['shift_x_values',['../root__utils_8h.html#a044952d2a3fb6224c1673d7af8259033',1,'shift_x_values(TGraph *gr, double shift):&#160;root_utils.cxx'],['../root__utils_8h.html#a441d312650ab64d6bd568196f7431945',1,'shift_x_values(TGraph *gr, double maxShift, int thisIdx, int nGraphs):&#160;root_utils.cxx']]],
  ['show_5fdoxygen',['show_doxygen',['../classhex__plotter.html#a61e183602f35108aeb3d882549e806da',1,'hex_plotter']]],
  ['sign',['sign',['../cpp__utils_8h.html#a2d0021d6dba0358706a3ce38e0f03d06',1,'cpp_utils.cxx']]],
  ['smear_5fbins',['smear_bins',['../root__utils_8h.html#a87e88a5485393ebe2fcca18793a69351',1,'smear_bins(TH1F *hist, TH1F *hUnc, int seed):&#160;root_utils.cxx'],['../root__utils_8h.html#a2c18706cf157b04e64c9ca97e2372ab3',1,'smear_bins(TH1F *, int seed=0):&#160;root_utils.cxx']]],
  ['split',['split',['../cpp__utils_8h.html#aa2f68b1e1cf6ea564b5d5c05d0c9dd9c',1,'cpp_utils.cxx']]],
  ['starts_5fwith',['starts_with',['../cpp__utils_8h.html#a44331d3bd328c0638b6425981520813c',1,'cpp_utils.cxx']]],
  ['stretch_5fpolygon',['stretch_polygon',['../root__utils_8h.html#aaa340edb1cce635446fb76803b86a214',1,'root_utils.cxx']]],
  ['string_5fto_5farray',['string_to_array',['../cpp__utils_8h.html#a4840830dbb731862bfc3874c3243256b',1,'cpp_utils.cxx']]],
  ['string_5fto_5farray_5fdouble',['string_to_array_double',['../cpp__utils_8h.html#aa310930cc8df39077ac41022acdebc60',1,'cpp_utils.cxx']]],
  ['strip_5fextension',['strip_extension',['../cpp__utils_8h.html#a042a92500e3d077d7bb3d93ac821ef48',1,'cpp_utils.cxx']]],
  ['switch_5fcolor',['switch_color',['../cpp__utils_8h.html#aab8656cc65bc85468f89d1dc9dbaa2e7',1,'cpp_utils.cxx']]]
];
