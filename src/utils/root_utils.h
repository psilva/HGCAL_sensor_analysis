/**
        @file root_utils.h
        @brief A collection of ROOT helper functions
  @author Andreas Alexander Maier
*/
#ifndef ROOT_UTILS_H
#define ROOT_UTILS_H

#include <fstream>
#include <iomanip>
#include <limits>
#include <sstream>
#include <utility>
#ifdef _WIN32
#include <stdint.h>
#endif

#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TMath.h"
#include "TMatrixD.h"
#include "TPad.h"
#include "TPaletteAxis.h"
#include "TPaveText.h"
#include "TPolyLine.h"
#include "TRandom.h"
#include "TSpline.h"
#include "TText.h"
#include "TTreePlayer.h"
#include "TVector2.h"
#include "TVirtualPad.h"
// #include "TEllipse.h"

#include "cpp_utils.h"

/// Returns Barlow correction factor for pseudo-experiments
double* get_barlow_corr_factor(double m, double n, double N, int verbose = 1, int replacement = 1);
/// Rounds a double to n counting digits
double round_to_n_digits(double value, int nDigits);
/// Rounds a double with uncertainty to n counting digits
void round_to_error_n_digits(double value, double unc, int nDigits, std::string& outputValue, std::string& outputUnc);
/// Displays how the above rounding function works
void rounding_example();
/// Determines the uncertainty of a difference of two values with uncertainties with optional scale factor
double unc_of_diff(double unc0, double unc1, double rho = 0, double factor = 1);
/**
        @brief Returns the uncertainty of a ratio

        https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Example_formulas
*/
double unc_of_ratio(double n, double N, double ne, double Ne, double corr, bool verbose = false);
/// Returns the uncertainty of a ratio by calling unc_of_ratio()
double unc_of_ratio(int n, int N, double corr, bool verbose);
/// Returns the uncertainty of a ratio unc_of_ratio()
double unc_of_ratio(int n, int N, bool verbose);
/// Calculates phi from x and y coordinates
double get_phi_from_xy(double x, double y);
/// Calculates the uncertainty of a correlation factor. Warning: get_corr_factor_unc not fully implemented yet!
double get_corr_factor_unc(TH2F* h);
/**
        @brief Returns quantity in smallest region containing frac of all events

        @param quant Quantity to be determined in the region, e.g. 'RMS' or 'Mean'
*/
double* get_min_hist_region(TH1F* h, double frac = 1.0, const std::string& quant = "RMS", bool verbose = false);
/// Incrementally calculates the mean
double incr_mean_calc(double oldMean, int newN, double newVal);
/// Incrementally calculates the standard deviation. Warning: not implemented yet!
double incr_STD_dev_calc(double oldSigma, double oldMean, int newN, double newVal);
/// Tests the functions incr_mean_calc() and incr_STD_dev_calc()
void RMS_test(int nmax);
/// Calculates delta R from angles
double get_delta_R(double eta0, double phi0, double eta1, double phi1);
/// Converts theta to eta
double get_eta_from_theta(double theta);
/// Converts eta to theta
double get_theta_from_eta(double eta);
/// Returns different settings for color, line and fill style for iterative assignment
int* get_color_line_fill_style(int number);
/// Returns different settings for color, line and fill style for iterative assignment without black
int* get_color_line_fill_style_no_black(int number);
/// Cuts off the last bins of a histogram
TH1F* cut_off_bins(TH1F* h, int nLastBins);
/// Saves a single pad from a canvas
void save_pad(TPad* pad, const char* saveName, int x = 700, int y = 500);
/// Saves a single pad from a canvas
void save_pad(TVirtualPad* pad, const char* saveName, int x = 700, int y = 500);
/// Sets negative bins to 0 and returns number of changed bins
int remove_negative_bins(TH1* hist);
/// Sets NAN bins to 0 and returns number of changed bins
int remove_NAN_bins(TH1* hist);
/// Sets OF bins to 0 and returns number of changed bins
int remove_OF_bins(TH1* hist, double OFvalue = MAX_DOUBLE);
/// Sets the histogram bins equal to their absolute value
void make_bins_absolute_values(TH1F* hist);
/// Sets the histogram bins equal to their square root
void make_bins_square_roots(TH1F* hist);
/**
  @brief Extends a histogram holding bin errors as contents

  Quadratically adds to error histogram hErr the symmetrized error taken from
  asymmetric up and dw variation histograms.

  @param noNormUnc drop normalization differences
*/
void add_symm_bin_err_from_diff(TH1F* hErr, TH1F* hOrig, TH1F* hVarUp, TH1F* hVarDw, bool noNormUnc = false);
/// Assigns uncertainties to graph based on two variation graphs
void set_bin_err_from_diff(TGraphAsymmErrors* grOrig, TGraph* grVar0, TGraph* grVar1);
/// Assigns uncertainties to histogram based on two variation histograms
void set_bin_err_from_diff(TH1F* hOrig, TH1F* hVar0, TH1F* hVar1, bool norm = false);
/// Returns the chi2 from comparison of two histograms
double get_hist_chi2_diff(TH1F* h1, TH1F* h2, bool norm = false, double xMin = 0, double xMax = 0);
/// Returns the chi2 from comparison of a histogram and a function
double get_hist_chi2_diff(TH1F* hist, TF1* func, bool norm = false, double xMin = 0, double xMax = 0);
/// Smear the bins of a histogram according to specified uncertainties
void smear_bins(TH1F* hist, TH1F* hUnc, int seed);
/// Smear the bins of a histogram according to square root uncertainties
void smear_bins(TH1F*, int seed = 0);
/// Display a matrix in stdout
void print_matrix(const TMatrixD& m, std::string format = "", const std::string& name = "", int colsPerSheet = 10);
/// Remove several rows and/or columns from a matrix
TMatrixD* remove_row_column(TMatrixD* m, std::vector<int> rows, std::vector<int> cols, bool verbose = false);
/// Remove a single row and/or column from a matrix
TMatrixD* remove_row_column(TMatrixD* m, int row = -1, int col = -1, int verbose = 1);
/// Convert a 2D matrix to a 2D histogram
TH2D* convert_matrix_to_hist(TMatrixD m);
/// Convert a 2D histogram to a 2D matrix
TMatrixD convert_hist_to_matrix(TH2D* h);
/// Scale the bins of the histogram by a factor
void scale_bins(TH1F* hist, double scale);
/// Returns the ratio of two functions
TH1F* function_ratio(TF1* f0, TF1* f1, double min, double max, int nPoints = 10000);
/// Returns the average absolute error of all histogram bins
double average_error_per_bin(TH1F* h);
/// Returns the cumulative signed residual of the histogram and the function
double cumulative_bin_residual(TH1F* h, TH1F* g);
/// Get max and min of array of graphs
double* get_plot_ranges(TGraphErrors** gr, int nGraphs = 1, int skipNGraphs = 0, bool extraTopSpace = false);
/// Get max and min of array of graphs
double* get_plot_ranges(TGraphErrors** gr, std::vector<int> onlyThose, bool extraTopSpace = false);
/// Get the maximum y value of a graph
double get_maximum(TGraph* gr, bool extraTopSpace = false);
/// Get the minimum y value of a graph
double get_minimum(TGraph* gr, bool extraBottomSpace = false);
/// Shifts the x values of a graph, useful for plotting overlaid graphs
void shift_x_values(TGraph* gr, double shift);
/// As shift_x_values() but generates a shift for iterative plotting
void shift_x_values(TGraph* gr, double maxShift, int thisIdx, int nGraphs);
/// Print tree contents to a txt file (still hardcoded branch names)
void print_TTree_to_txt(const std::string& fileName, int maxEvts = 0);
/// Returns x range ((xup-xdw)/2) around bin with compatible bin content
double bin_positive_error(TH1F* hist, int bin);
/// Returns the Full Width at Half Maximum (FWHM) of the histogram
double get_FWHM(TH1F* hist);
/// Returns the uncertainty of the FWHM of the histogram
double get_FWHM_unc(TH1F* hist);
/// Prints the bins of the histogram
void print_bins(TH1F* hist);
/// Returns the bin by bin ratio of two histograms
TH2F* divide_hists(TH2F* h, TH2F* g);
/// Sets the bins compatible with 1 to one, used for 2D ratio plots.
void set_close_bins_to_one(TH2F* hist);
/// Sets the maximum of h to the maximum of h and h1
void set_max_of_hists(TH1* h = nullptr, TH1* h1 = nullptr);
/// Removes an object from a root file
void delete_obj_from_file(const std::string& fileName, const std::string& objectName);
/// Rotate a 2D histogram by 90 degrees
TH2D* rotate_90_degrees(TH2D* h);
/// Returns a histogram randomized from h with specified number of entries
TH1F* randomize_hist(TH1F* h, int entries = -1);
/// Replace the contents of one histogram with another
void replace_contents(TH1* hOrig, TH1* hNew);
/// Draw a white bin at specified bin position in histogram
void draw_white_bin(int iBinX, int iBinY, TH2D* h);
/// Special fill function where over- and underflow are filled into the first and last bin
void FILL(TH1F* hist, double x, double w);
/// Returns the ratio of two histograms
TH1F* get_ratio(TH1F* hData, TH1F* hMC, int color = -1, double min = 0.9, double max = 1.1, bool normalized = false,
                double corrSingleBins = 0);
/// Assigns Poisson errors to the histogram
TGraphAsymmErrors* poissonize(TH1* h);
/// Helper function for poissonize()
double GC_up(double hData);
/// Helper function for poissonize()
double GC_down(double hData);
/// Assign asymmetric up and dw variation as uncertainty
TGraphAsymmErrors* asymm_bin_unc_graph(TH1F* hOrig, TH1F* hVarUp, TH1F* hVarDw);
/**
        @brief Performs a number of tests to assess the compatibility of histograms

        @param textXPos x position of the information to be printed at in a plot
        @param displayChi2 Optional parameter to print the results as pave in a plot
        @param nFuncPar Optional parameter to subtract from the number of degrees of freedom
        @param hInvCov Optional parameter to provide a covariance matrix for the two histograms
*/
void check_hist_compatibility(TH1F* hData, TH1F* hMC, double textXPos = 0.12, bool displayChi2 = true, float xMin = 0,
                              float xMax = 0, int nFuncPar = 0, TH2D* hInvCov = nullptr);
/// Like check_hist_compatibility() but for a histogram and a function
void check_hist_compatibility(TH1F*, TF1*, double textXPos = 0.12, bool displayChi2 = true, float xMin = -1,
                              float xMax = -1);
/// Returns the projection of a 1D histogram on its y-axis
TH1F* project_to_y_axis(TH1F* h, bool noEmptyBins = true, double maxY = 0);
/// Returns mean, mean error and RMS of the average bin content from calling project_to_y_axis()
double* get_average_bin_content(TH1F* h);
/// Returns the integral of a graph
double graph_integral(TGraph* g);
/// Returns the y value of a graph of a point
double get_graph_value(TGraph* g, int point);
/// Returns y value and its uncertainty of a graph point as array
double* get_graph_point(TGraphErrors* g, int point);
/// Returns y value and its uncertainty up and down of a graph point as array
double* get_graph_point(TGraphAsymmErrors* g, int point);
/// Helper function to draw a nicely formatted 2D histogram
void draw_TH2F(TH2F* h, const std::string& drawoption = "colz");
/// Shifts the palette axis of a 2D histogram
void shift_palette_axis(TH2F* h, double xShift = 0);
/// Resizes the palette axis of a 2D histogram
void resize_palette_axis(TH2F* h, double x0, double x1);
/**
        @brief Rotates a polygon

        - angle > 0: rotation
        - angle < 0: invert x, then rotate
*/
void rotate_polygon(TPolyLine* pline, double xCenter, double yCenter, double angle);
/// Bend a polygon
void bend_polygon(TPolyLine* pline, double strength, int granularity = 6);
/// Fill lines between polygon points with additional points
void enrich_polygon(TPolyLine* pline, int granularity);
/// Invert a polygon
void invert_polygon(TPolyLine* pline, double xCenter, double yCenter);
/// Stretch a polygon by factors in x and y direction
void stretch_polygon(TPolyLine* pline, double xStretch = 1, double yStretch = 1);
/// Shear a polygon by factor in x and y direction
void shear_polygon(TPolyLine* pline, double xFactor = 0, double yFactor = 0);
/// Return index of polygon point matching x and y
int get_polygon_point(TPolyLine* pline, double x, double y);
/// Crop a polygon at fractional size in x and y direction
void crop_polygon(TPolyLine* pline, double xFrac = 0, double yFrac = 0);
/// Remvove all points above a certain parameter value on axis
void crop_polygon_above(TPolyLine* pline, double val, const std::string& axis = "X");
/// Remove consecutively identical points from plygon
void remove_duplicates(TPolyLine* pline);
/// Remove point at index from a polygon
void remove_polygon_point(TPolyLine* pline, int idx = -1);
/// Insert a point into a polygon after index
void insert_polygon_point(TPolyLine* pline, double x, double y, int idx = -1);
/// Replace a point at index in a polygon
void replace_polygon_point(TPolyLine* pline, double x, double y, int idx = -1);
/// Returns the center of gravity of a polygon around a given point
double* get_poly_center_of_gravity(TPolyLine* pline, bool dropLastPoint = false);
/// Returns smallest x value with a y value between xMin and xMax
double find_first_x_above(TSpline* spline, double yValue, double xMin, double xMax);
/// Checks a histogram for outlier points, i.e. sudden jumps in value
bool check_hist_for_outliers(TH1F* h, int verbose = 1, double sigmaThreshold = 5);
/// Helper function to draw a pave object with several lines of text
void draw_TPave(std::vector<std::string> lines, double x1 = 0.22, double y1 = 0.83, double x2 = 0.3, double y2 = 0.92,
                double textSize = 0.03, const std::string& option = "NDC", int color = 1);
/// Helper function to draw a pave object with on line of text
void draw_TPave(const std::string& line, double x1 = 0.22, double y1 = 0.83, double x2 = 0.3, double y2 = 0.92,
                double textSize = 0.03, const std::string& option = "NDC", int color = 1);
/// Helper function to make a label in a plot
TPaveText* make_label(double x1, double y1, double x2, double y2, double size = 0.03, double align = 32);
/// Transform bins on axis to be equidistant in log scale
void make_log_bins(TH1* h, const std::string& axis = "x");
/// Draw dashed lines at bin boundaries
void draw_bin_grid(TH1* h, const std::string& axis = "x", int lineStyle = 3, int lineColor = 1, int lineWidth = 1);
/// Get absolute position of pad on canvas. Returns [xlow, ylow, xup, yup]
double* get_pad_boundary(TPad* pad);
/// Prints some quantities for a histogram for equality testing
std::string unique_ID(TH1* h);
#endif
