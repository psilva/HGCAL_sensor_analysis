# Utilities
SET(UTILITY_SOURCES
    utils/cpp_utils.cxx
    utils/fit_utils.cxx
    utils/root_utils.cxx
)

# Geo Plotter Library:
SET(GEOPLOTTER_SOURCES
    geo_plot/geo_plot.cxx
)

# Add target for geo plotter:
ADD_LIBRARY(GeoPlotter SHARED ${GEOPLOTTER_SOURCES} ${UTILITY_SOURCES})
TARGET_LINK_LIBRARIES(GeoPlotter ${ROOT_LIBRARIES} ${ROOT_COMPONENT_LIBRARIES})

# Collect all source files of the executable
FILE(GLOB EXEC_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/exec/*.cxx)

# Add the build target and link the ROOT libraries:
ADD_EXECUTABLE(${PROJECT_NAME} ${EXEC_SOURCES})
TARGET_LINK_LIBRARIES(${PROJECT_NAME} GeoPlotter ${ROOT_LIBRARIES} ${ROOT_COMPONENT_LIBRARIES})

INSTALL(TARGETS ${PROJECT_NAME} GeoPlotter
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)
