/**
        @file hex_values.h
        @brief Reads data files and prepare them for HexPlot
  @author Andreas Alexander Maier
*/
#ifndef HEX_VALUES_H
#define HEX_VALUES_H

#include <algorithm>
#include <limits>
#include <utility>

#include "TArrow.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TSpline.h"

#include "../utils/cpp_utils.h"
#include "../utils/fit_functions.h"
#include "../utils/fit_utils.h"
#include "globals.h"
#include "hex_map.h"

/// Reads and generates the values to be plotted later
class hex_values {
 private:
  const double ALLPAD_SELECTOR = -10000000;

  bool invertSelectors;
  bool isAverageValues;
  bool detailedPlots;
  bool detailedPlotsDrawn;
  bool isInitialized;
  bool singlePad;
  bool testInfo;
  bool noTrafo;
  int verbose;
  int appearance;
  int global_maxpad;
  int global_minpad;
  int maxpad[MAX_SELECTOR_NUMBER];
  int minpad[MAX_SELECTOR_NUMBER];
  int nVolts;
  int highestInterPad;
  double yScale;
  double selector;
  double effectiveSelector;  ///< = selector, except in case selector unspecified, then it is highest found selector
  double minNonZeroValue;
  double maxNonZeroValue;
  double zoomMin;
  double zoomMax;
  std::map<std::string, int> cumulativePlotsMap;
  std::map<std::string, int> currentCanvIdxMap;
  std::map<std::string, int> currentCanvStartIdxMap;
  std::map<std::string, std::vector<TCanvas*>> detailedCanvMap;
  std::vector<bool> isPadInDataFile;
  std::vector<std::vector<int>> isPadFilled;
  std::vector<double> volt_vec;
  std::string valueOption;
  std::string inputFile;
  std::string inputFormat;
  std::string mapFile;
  std::string fitFunc;
  std::string valueName;
  std::string valueUnit;
  std::string selectorName;
  std::string selectorUnit;
  TH2F* values_h2d;
  TH2F* inter0_h2d;
  TH2F* inter1_h2d;
  TH2F* inter2_h2d;
  TH2F* inter3_h2d;
  TH2F* inter4_h2d;
  TH2F* inter5_h2d;
  TH2F* inter6_h2d;
  TH2F* inter7_h2d;
  TH2F* inter8_h2d;
  TH2F* inter9_h2d;

  /// Reads the data files passed by the user and provides them to the other functions
  int read_data(const std::string& thisInputFile);
  /// Clears the data vectors
  void clear_data();
  /**
  \brief Loads the values correpsonding to a given voltage

  Loads the values corresponding to a given voltage from an input file and returns
  an array of histograms with the values for the cells and their inter-cells.

  \returns An array of histograms with element 0 being the cell values and
  elements 1 to 10 possible inter-cell values.
  */
  TObject** load_values(const std::string& thisInputFile, double selector);
  /**
  \brief Averages values of several input files

  Calls load_values() for every input file and returns the average

  \returns A histograms with the average cell values
  */
  TObject* load_value_average(double selector);
  /**
  \brief Processes the raw data values to derive a new quantity, e.g. a depletion voltage

  Loads all values for all voltages by repeatedly calling load_values(). Then
  performs the specified operation for every cell. The result is returned.

  \returns A histograms with the derived value for every pad
  */
  TObject* load_derived_values(const std::string& thisInputFile);
  /**
  \brief Averages derived values of several input files

  Calls load_derived_values() for every input file and returns the average

  \returns A histograms with the average pad derived values
  */
  TObject* load_derived_value_average();
  /// A wrapper for all functions loading values
  TObject** load_data_histo(bool is1D);
  /// Fill plots into several canvases
  void plot_this_pad(TCanvas* singleCanv, const std::string& saveName, bool isFinal,
                     const std::string& description = "details", int startIndex = MAX_INTEGER,
                     int endIndex = MAX_INTEGER);
  /// Plots detailed pad plots, e.g. IV or CV curves for every pad
  void plot_detailed_plots(TH2F* h_fill, const std::string& fileName);
  /// Determines values derived from pad value curves (e.g. IV/CV curves) using a dedicated fit function
  double* fit_pad_value_curve(TGraphErrors* gr_values, TF1** f_fitFunc, const std::string& fitFunc, int verbose);
  /// Determines values derived from pad value curves (e.g. IV/CV curves) using spline operations
  double* fit_pad_value_curve(TGraphErrors* gr_values, TSpline5** s_spline, const std::string& fitFunc, int verbose);

 public:
  void invert_selector(bool p = true) { invertSelectors = p; };
  void set_average(bool p) { isAverageValues = p; };
  void set_appearance(int p) { appearance = p; };
  void set_detailed_plots(bool p) { detailedPlots = p; };
  void set_fit_func(std::string p) { fitFunc = std::move(p); };
  void set_input_file(std::string p) { inputFile = std::move(p); };
  void set_input_format(std::string p) { inputFormat = std::move(p); };
  void set_map_file(std::string p) { mapFile = std::move(p); };
  void set_no_trafo(bool p) { noTrafo = p; };
  void set_range(double min = 0, double max = 0) {
    zoomMin = min;
    zoomMax = max;
  };
  void set_selector(double p) { selector = p; };
  void set_selector_name(std::string name, std::string unit) {
    selectorName = std::move(name);
    selectorUnit = std::move(unit);
  };
  double get_selector() { return effectiveSelector; };
  void set_single_pad(bool p) { singlePad = p; };
  /// Print out hashed info on result
  void set_test_info(bool p) { testInfo = p; };
  void set_value_name(std::string name, std::string unit) {
    valueName = std::move(name);
    valueUnit = std::move(unit);
  };
  void set_value_option(std::string p) { valueOption = std::move(p); };
  void set_verbose(int p) { verbose = p; };
  void set_y_scale(double p) { yScale = p; };

  /// Returns the data of the cell contents as a 1D histogram
  TH1F* load_data_histo_1d() { return dynamic_cast<TH1F*>(load_data_histo(true)[0]); };
  /// Returns the data of cell contents and its interpads as array of 1D histograms
  TH1F** load_data_histo_1d_interpad() { return reinterpret_cast<TH1F**>(load_data_histo(true)); };  // NOLINT
  /// Returns the data of the cell contents for all voltages and pads as 2D histogram
  TH2F* load_data_histo_2d() { return dynamic_cast<TH2F*>(load_data_histo(false)[0]); };
  /// Constructor
  hex_values();
};

#endif
