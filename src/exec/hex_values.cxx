#include "hex_values.h"

int N_VALID_FILES;
std::string OUTPUT_DIR;
std::string VALID_SENSOR_NAMES[MAX_SENSOR_NUMBER];
TGraph *GR_CONTENTS[MAX_CELL_NUMBER];
double SENSOR_VALS_FOR_AVERAGE[MAX_SENSOR_NUMBER][MAX_CELL_NUMBER];

hex_values::hex_values() {
  invertSelectors = false;
  detailedPlots = false;
  detailedPlotsDrawn = false;
  isAverageValues = false;
  isInitialized = false;
  testInfo = false;
  singlePad = false;
  noTrafo = false;
  inputFile = "";
  inputFormat = "";
  mapFile = "";
  valueOption = "";
  fitFunc = "linlin";
  valueName = "";
  valueUnit = "";
  selectorName = "";
  selectorUnit = "";
  yScale = 1;
  verbose = 0;
  appearance = -1;
  zoomMin = 0;
  zoomMax = 0;
  selector = AUTOPAD_SELECTOR;
  effectiveSelector = AUTOPAD_SELECTOR;
  for (size_t i = 0; i < MAX_CELL_NUMBER; i++) {
    for (auto &j : SENSOR_VALS_FOR_AVERAGE) {
      j[i] = 0;
    }
  }
  values_h2d = nullptr;
  inter0_h2d = nullptr;
  inter1_h2d = nullptr;
  inter2_h2d = nullptr;
  inter3_h2d = nullptr;
  inter4_h2d = nullptr;
  inter5_h2d = nullptr;
  inter6_h2d = nullptr;
  inter7_h2d = nullptr;
  inter8_h2d = nullptr;
  inter9_h2d = nullptr;
  for (int i = 0; i < MAX_SELECTOR_NUMBER; ++i) {
    maxpad[i] = -1;
    minpad[i] = MAX_INTEGER;
    std::vector<int> v_tmp;
    for (int j = 0; j < MAX_CELL_NUMBER; ++j) {
      v_tmp.push_back(0);
    }
    isPadFilled.push_back(v_tmp);
  }
  for (int j = 0; j < MAX_CELL_NUMBER; ++j) {
    isPadInDataFile.push_back(false);
  }
  highestInterPad = -1;
  global_maxpad = 0;
  global_minpad = MAX_INTEGER;
  nVolts = -1;
  minNonZeroValue = MAX_DOUBLE;
  maxNonZeroValue = -MAX_DOUBLE;
}
void hex_values::clear_data() {
  // clang-format off
  if (values_h2d != nullptr) { values_h2d->Reset(); }
  if (inter0_h2d != nullptr) { inter0_h2d->Reset(); }
  if (inter1_h2d != nullptr) { inter1_h2d->Reset(); }
  if (inter2_h2d != nullptr) { inter2_h2d->Reset(); }
  if (inter3_h2d != nullptr) { inter3_h2d->Reset(); }
  if (inter4_h2d != nullptr) { inter4_h2d->Reset(); }
  if (inter5_h2d != nullptr) { inter5_h2d->Reset(); }
  if (inter6_h2d != nullptr) { inter6_h2d->Reset(); }
  if (inter7_h2d != nullptr) { inter7_h2d->Reset(); }
  if (inter8_h2d != nullptr) { inter8_h2d->Reset(); }
  if (inter9_h2d != nullptr) { inter9_h2d->Reset(); }
  // clang-format on
  for (int i = 0; i < MAX_SELECTOR_NUMBER; ++i) {
    maxpad[i] = -1;
    minpad[i] = MAX_INTEGER;
    for (int j = 0; j < MAX_CELL_NUMBER; ++j) {
      isPadFilled.at(i).at(j) = 0;
    }
  }
  for (int j = 0; j < MAX_CELL_NUMBER; ++j) {
    isPadInDataFile.push_back(false);
  }
  highestInterPad = -1;
  global_maxpad = 0;
  global_minpad = MAX_INTEGER;
}
int hex_values::read_data(const std::string &thisInputFile) {
  if (verbose != 0) {
    printf("Executing read_data_files...\n");
  }
  if (thisInputFile == "") {
    printf("Warning: no input file provided!\n");
    return 1;
  }

  clear_data();

  // reading from file into tree
  auto *tree_val = new TTree("tree_val", "tree_val");
  std::vector<std::string> inputFileList = split(thisInputFile, ',');
  for (unsigned int i = 0; i < inputFileList.size(); ++i) {
    // Silence warnings during TTree::ReadFile
    inputFormat = replace_string_all(inputFormat, ":", "/D:");
    inputFormat.append("/D");
    Int_t currentIgnoreLevel = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kError;
    if (i == 0) {
      tree_val->ReadFile(inputFileList.at(i).c_str(), inputFormat.c_str());
    } else {
      tree_val->ReadFile(inputFileList.at(i).c_str());
    }
    // Restore previous settings
    gErrorIgnoreLevel = currentIgnoreLevel;
  }
  double padNumDataFile(0), Value(0), ValueError(0), Voltage(0), inter0(0), inter1(0), inter2(0), inter3(0), inter4(0),
      inter5(0), inter6(0), inter7(0), inter8(0), inter9(0);
  tree_val->SetBranchAddress("PADNUM", &padNumDataFile);
  tree_val->SetBranchAddress("VAL", &Value);
  // clang-format off
  if (tree_val->FindBranch("SELECTOR") != nullptr) { tree_val->SetBranchAddress("SELECTOR", &Voltage); }
  if (tree_val->FindBranch("VALERR") != nullptr) { tree_val->SetBranchAddress("VALERR", &ValueError); }
  if (tree_val->FindBranch("INTER0") != nullptr) {tree_val->SetBranchAddress("INTER0", &inter0); ++highestInterPad; }
  if (tree_val->FindBranch("INTER1") != nullptr) {tree_val->SetBranchAddress("INTER1", &inter1); ++highestInterPad; }
  if (tree_val->FindBranch("INTER2") != nullptr) {tree_val->SetBranchAddress("INTER2", &inter2); ++highestInterPad; }
  if (tree_val->FindBranch("INTER3") != nullptr) {tree_val->SetBranchAddress("INTER3", &inter3); ++highestInterPad; }
  if (tree_val->FindBranch("INTER4") != nullptr) {tree_val->SetBranchAddress("INTER4", &inter4); ++highestInterPad; }
  if (tree_val->FindBranch("INTER5") != nullptr) {tree_val->SetBranchAddress("INTER5", &inter5); ++highestInterPad; }
  if (tree_val->FindBranch("INTER6") != nullptr) {tree_val->SetBranchAddress("INTER6", &inter6); ++highestInterPad; }
  if (tree_val->FindBranch("INTER7") != nullptr) {tree_val->SetBranchAddress("INTER7", &inter7); ++highestInterPad; }
  if (tree_val->FindBranch("INTER8") != nullptr) {tree_val->SetBranchAddress("INTER8", &inter8); ++highestInterPad; }
  if (tree_val->FindBranch("INTER9") != nullptr) {tree_val->SetBranchAddress("INTER9", &inter9); ++highestInterPad; }
  // clang-format on

  // prepare variables to fill
  int cellAppearanceCount[MAX_CELL_NUMBER];
  for (int &j : cellAppearanceCount) {
    j = 0;
  }

  hex_map *hexMap = new hex_map(mapFile, verbose);

  std::vector<std::vector<double>> val_vec;
  std::vector<std::vector<double>> err_vec;
  std::vector<std::vector<double>> inter0_val_vec;
  std::vector<std::vector<double>> inter1_val_vec;
  std::vector<std::vector<double>> inter2_val_vec;
  std::vector<std::vector<double>> inter3_val_vec;
  std::vector<std::vector<double>> inter4_val_vec;
  std::vector<std::vector<double>> inter5_val_vec;
  std::vector<std::vector<double>> inter6_val_vec;
  std::vector<std::vector<double>> inter7_val_vec;
  std::vector<std::vector<double>> inter8_val_vec;
  std::vector<std::vector<double>> inter9_val_vec;
  for (int i = 0; i < MAX_CELL_NUMBER; ++i) {
    std::vector<double> tmp_vec;
    for (int j = 0; j < MAX_SELECTOR_NUMBER; ++j) {
      tmp_vec.push_back(0);
    }
    val_vec.push_back(tmp_vec);
    err_vec.push_back(tmp_vec);
    inter0_val_vec.push_back(tmp_vec);
    inter1_val_vec.push_back(tmp_vec);
    inter2_val_vec.push_back(tmp_vec);
    inter3_val_vec.push_back(tmp_vec);
    inter4_val_vec.push_back(tmp_vec);
    inter5_val_vec.push_back(tmp_vec);
    inter6_val_vec.push_back(tmp_vec);
    inter7_val_vec.push_back(tmp_vec);
    inter8_val_vec.push_back(tmp_vec);
    inter9_val_vec.push_back(tmp_vec);
  }

  // loop tree and fill values
  for (int i = 0; i < tree_val->GetEntries(); ++i) {
    tree_val->GetEntry(i);
    if (invertSelectors) {
      Voltage = -Voltage;
    }
    if (verbose > 1) {
      printf("Reading raw value for pad %d and voltage %.1f: %.3e +- %.3e\n", int(padNumDataFile), Voltage, Value,
             ValueError);
    }
    int voltPos = find(volt_vec.begin(), volt_vec.end(), Voltage) - volt_vec.begin();
    if (!is_in_vector(Voltage, volt_vec)) {
      volt_vec.push_back(Voltage);
      voltPos = volt_vec.size() - 1;
    }
    // printf("Entry %d: volt %.2f volt position in vector %d padnum %.0f value %f\n",i, Voltage, voltPos,
    // padNumDataFile, Value);
    int geoNum = hexMap->geo_file_number(padNumDataFile);
    if (geoNum == -1) {
      continue;
    }
    if (appearance >= 0 && cellAppearanceCount[int(padNumDataFile)] > appearance) {
      continue;
    }
    if (Value == SKIP_VALUE) {
      continue;
    }
    ++cellAppearanceCount[int(padNumDataFile)];
    if (maxpad[voltPos] < geoNum) {
      maxpad[voltPos] = geoNum;
    }
    if (minpad[voltPos] > geoNum) {
      minpad[voltPos] = geoNum;
    }
    // printf("Current padNumDataFile %d corresponds to geoNum %d max %d min %d\n", int(padNumDataFile), geoNum,
    // maxpad[voltPos],minpad[voltPos] );
    isPadFilled[voltPos][geoNum] = 1;
    isPadInDataFile.at(geoNum) = true;
    val_vec.at(geoNum).at(voltPos) = Value * yScale;
    err_vec.at(geoNum).at(voltPos) = ValueError * yScale;
    // clang-format off
    if (highestInterPad >= 0) { inter0_val_vec.at(geoNum).at(voltPos) = inter0 * yScale; }
    if (highestInterPad >= 1) { inter1_val_vec.at(geoNum).at(voltPos) = inter1 * yScale; }
    if (highestInterPad >= 2) { inter2_val_vec.at(geoNum).at(voltPos) = inter2 * yScale; }
    if (highestInterPad >= 3) { inter3_val_vec.at(geoNum).at(voltPos) = inter3 * yScale; }
    if (highestInterPad >= 4) { inter4_val_vec.at(geoNum).at(voltPos) = inter4 * yScale; }
    if (highestInterPad >= 5) { inter5_val_vec.at(geoNum).at(voltPos) = inter5 * yScale; }
    if (highestInterPad >= 6) { inter6_val_vec.at(geoNum).at(voltPos) = inter6 * yScale; }
    if (highestInterPad >= 7) { inter7_val_vec.at(geoNum).at(voltPos) = inter7 * yScale; }
    if (highestInterPad >= 8) { inter8_val_vec.at(geoNum).at(voltPos) = inter8 * yScale; }
    if (highestInterPad >= 9) { inter9_val_vec.at(geoNum).at(voltPos) = inter9 * yScale; }
    // clang-format on

    if (fabs(val_vec.at(geoNum).at(voltPos)) < MAX_ALLOWED_PAD_CONTENT) {
      if ((val_vec.at(geoNum).at(voltPos) != 0.0) && val_vec.at(geoNum).at(voltPos) < minNonZeroValue) {
        minNonZeroValue = val_vec.at(geoNum).at(voltPos);
      }
      if ((val_vec.at(geoNum).at(voltPos) != 0.0) && val_vec.at(geoNum).at(voltPos) > maxNonZeroValue) {
        maxNonZeroValue = val_vec.at(geoNum).at(voltPos);
      }
    }
  }

  for (int iVolt = 0; iVolt < MAX_SELECTOR_NUMBER; ++iVolt) {
    // printf("Volt %d Max bin %d Min bin %d \n", iVolt,maxpad[iVolt],minpad[iVolt] );
    if (maxpad[iVolt] > global_maxpad) {
      global_maxpad = maxpad[iVolt];
    }
    if (minpad[iVolt] < global_minpad) {
      global_minpad = minpad[iVolt];
    }
  }

  nVolts = volt_vec.size();
  if (nVolts == 0) {
    printf("Warning! No voltages found in file! Maybe wrong format for TTree::ReadFile()?\n");
  }
  if (verbose > 0) {
    printf("Found these voltages in file:\n");
    for (int i = 0; i < nVolts; ++i) {
      printf("%d.: %.2f\n", i, volt_vec.at(i));
    }
  }

  if (!singlePad && valueOption != "DEP" && valueOption != "VAL" && selector != AUTOPAD_SELECTOR &&
      selector != ALLPAD_SELECTOR && std::find(volt_vec.begin(), volt_vec.end(), selector) == volt_vec.end()) {
    printf("Warning: no entries in data file matching voltage %.2e!\n", selector);
  }
  if (selector == AUTOPAD_SELECTOR && valueOption != "DEP" && valueOption != "VAL") {
    printf("Warning: no voltage given, select the one with highest absolute value!\n");
    selector = 0;
    for (double i : volt_vec) {
      if (fabs(selector) < fabs(i)) {
        selector = i;
      }
    }
    effectiveSelector = selector;
  }

  // load all values in 2D histogram
  std::string axisTitles = Form(";Pad number;%s [%s];%s [%s]", selectorName.c_str(), selectorUnit.c_str(),
                                valueName.c_str(), valueUnit.c_str());
  // clang-format off
  values_h2d = new TH2F("values_h2d", Form("values_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter0_h2d = new TH2F("inter0_h2d", Form("inter0_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter1_h2d = new TH2F("inter1_h2d", Form("inter1_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter2_h2d = new TH2F("inter2_h2d", Form("inter2_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter3_h2d = new TH2F("inter3_h2d", Form("inter3_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter4_h2d = new TH2F("inter4_h2d", Form("inter4_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter5_h2d = new TH2F("inter5_h2d", Form("inter5_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter6_h2d = new TH2F("inter6_h2d", Form("inter6_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter7_h2d = new TH2F("inter7_h2d", Form("inter7_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter8_h2d = new TH2F("inter8_h2d", Form("inter8_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  inter9_h2d = new TH2F("inter9_h2d", Form("inter9_h2d%s", axisTitles.c_str()), global_maxpad, 0.5, global_maxpad + 0.5, nVolts, 0, nVolts);
  // clang-format on
  for (int i_volt = 0; i_volt < nVolts; ++i_volt) {
    for (int i_pad = global_minpad; i_pad <= global_maxpad; ++i_pad) {
      if (!isPadInDataFile.at(i_pad)) {
        continue;
      }
      double fillval = val_vec[i_pad][i_volt];
      double fillerr = err_vec[i_pad][i_volt];
      if (isPadFilled[i_volt][i_pad] == 0) {
        fillval = std::numeric_limits<double>::quiet_NaN();
      }
      if (verbose > 1) {
        printf("Setting bin %d voltage %.0f value to %.2e +- %.2e\n", i_pad + 1, volt_vec.at(i_volt), fillval, fillerr);
      }
      values_h2d->SetBinContent(i_pad + 1, i_volt + 1, fillval);
      values_h2d->SetBinError(values_h2d->FindBin(i_pad + 1, i_volt), fillerr);
      inter0_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter0_val_vec[i_pad][i_volt]);
      inter1_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter1_val_vec[i_pad][i_volt]);
      inter2_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter2_val_vec[i_pad][i_volt]);
      inter3_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter3_val_vec[i_pad][i_volt]);
      inter4_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter4_val_vec[i_pad][i_volt]);
      inter5_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter5_val_vec[i_pad][i_volt]);
      inter6_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter6_val_vec[i_pad][i_volt]);
      inter7_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter7_val_vec[i_pad][i_volt]);
      inter8_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter8_val_vec[i_pad][i_volt]);
      inter9_h2d->SetBinContent(i_pad + 1, i_volt + 1, inter9_val_vec[i_pad][i_volt]);
    }
    values_h2d->GetYaxis()->SetBinLabel(i_volt + 1, Form("%.0f", volt_vec.at(i_volt)));
  }

  if (detailedPlots) {
    plot_detailed_plots(values_h2d, inputFileList.at(0));
  }

  if (verbose != 0) {
    printf("load_histogram finished\n");
  }
  if (testInfo) {
    printf("hex_values::read_data %s\n", unique_ID(values_h2d).c_str());
  }
  return 0;
}
TObject **hex_values::load_values(const std::string &thisInputFile, double selector) {
  if (verbose != 0) {
    printf("Executing load_values...\n");
  }

  if (thisInputFile == "") {
    printf("Warning: no input file provided!\n");
    return nullptr;
  }

  read_data(thisInputFile);
  if (selector == AUTOPAD_SELECTOR) {
    selector = effectiveSelector;
  }

  // now fill histograms to be returned
  auto **final_obj_1d = new TH1F *[11];
  auto **final_obj_2d = new TH2F *[11];

  if (singlePad) {
    auto singleChan = int(selector);
    if (verbose > 0) {
      printf("Setting values for pad %d and all voltages\n", singleChan);
    }
    auto *values_h1d = new TH1F("values_h1d", "values_h1d", nVolts, 0, nVolts);
    auto *inter0_h1d = new TH1F("inter0_h1d", "inter0_h1d", nVolts, 0, nVolts);
    auto *inter1_h1d = new TH1F("inter1_h1d", "inter1_h1d", nVolts, 0, nVolts);
    auto *inter2_h1d = new TH1F("inter2_h1d", "inter2_h1d", nVolts, 0, nVolts);
    auto *inter3_h1d = new TH1F("inter3_h1d", "inter3_h1d", nVolts, 0, nVolts);
    auto *inter4_h1d = new TH1F("inter4_h1d", "inter4_h1d", nVolts, 0, nVolts);
    auto *inter5_h1d = new TH1F("inter5_h1d", "inter5_h1d", nVolts, 0, nVolts);
    auto *inter6_h1d = new TH1F("inter6_h1d", "inter6_h1d", nVolts, 0, nVolts);
    auto *inter7_h1d = new TH1F("inter7_h1d", "inter7_h1d", nVolts, 0, nVolts);
    auto *inter8_h1d = new TH1F("inter8_h1d", "inter8_h1d", nVolts, 0, nVolts);
    auto *inter9_h1d = new TH1F("inter9_h1d", "inter9_h1d", nVolts, 0, nVolts);
    for (int i = 0; i < nVolts; ++i) {
      if (verbose > 1) {
        printf("Setting bin %d value to %.2e +- %.2e (singleChan >= 0)\n", i,
               values_h2d->GetBinContent(singleChan + 1, i + 1), values_h2d->GetBinError(singleChan + 1, i + 1));
        if (highestInterPad > -1) {
          printf("Inter-pad values: %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e\n",
                 inter0_h2d->GetBinContent(singleChan + 1, i + 1), inter1_h2d->GetBinContent(singleChan + 1, i + 1),
                 inter2_h2d->GetBinContent(singleChan + 1, i + 1), inter3_h2d->GetBinContent(singleChan + 1, i + 1),
                 inter4_h2d->GetBinContent(singleChan + 1, i + 1), inter5_h2d->GetBinContent(singleChan + 1, i + 1),
                 inter6_h2d->GetBinContent(singleChan + 1, i + 1), inter7_h2d->GetBinContent(singleChan + 1, i + 1),
                 inter8_h2d->GetBinContent(singleChan + 1, i + 1), inter9_h2d->GetBinContent(singleChan + 1, i + 1));
        }
      }
      values_h1d->SetBinContent(i + 1, values_h2d->GetBinContent(singleChan + 1, i + 1));
      values_h1d->SetBinError(i + 1, values_h2d->GetBinError(singleChan + 1, i + 1));
      values_h1d->GetXaxis()->SetBinLabel(i + 1, Form("%.0f", volt_vec.at(i)));
      inter0_h1d->SetBinContent(i + 1, inter0_h2d->GetBinContent(singleChan + 1, i + 1));
      inter1_h1d->SetBinContent(i + 1, inter1_h2d->GetBinContent(singleChan + 1, i + 1));
      inter2_h1d->SetBinContent(i + 1, inter2_h2d->GetBinContent(singleChan + 1, i + 1));
      inter3_h1d->SetBinContent(i + 1, inter3_h2d->GetBinContent(singleChan + 1, i + 1));
      inter4_h1d->SetBinContent(i + 1, inter4_h2d->GetBinContent(singleChan + 1, i + 1));
      inter5_h1d->SetBinContent(i + 1, inter5_h2d->GetBinContent(singleChan + 1, i + 1));
      inter6_h1d->SetBinContent(i + 1, inter6_h2d->GetBinContent(singleChan + 1, i + 1));
      inter7_h1d->SetBinContent(i + 1, inter7_h2d->GetBinContent(singleChan + 1, i + 1));
      inter8_h1d->SetBinContent(i + 1, inter8_h2d->GetBinContent(singleChan + 1, i + 1));
      inter9_h1d->SetBinContent(i + 1, inter9_h2d->GetBinContent(singleChan + 1, i + 1));
    }
    final_obj_1d[0] = values_h1d;
    final_obj_1d[1] = inter0_h1d;
    final_obj_1d[2] = inter1_h1d;
    final_obj_1d[3] = inter2_h1d;
    final_obj_1d[4] = inter3_h1d;
    final_obj_1d[5] = inter4_h1d;
    final_obj_1d[6] = inter5_h1d;
    final_obj_1d[7] = inter6_h1d;
    final_obj_1d[8] = inter7_h1d;
    final_obj_1d[9] = inter8_h1d;
    final_obj_1d[10] = inter9_h1d;
    if (verbose != 0) {
      printf("load_values finished!\n");
    }
    return reinterpret_cast<TObject **>(final_obj_1d);  // NOLINT
  }
  if (selector == ALLPAD_SELECTOR) {
    if (verbose > 0) {
      printf("Setting values for all pads and all voltages\n");
    }
    if (verbose > 0) {
      printf("Histogram has %d xbins and %d ybins\n", values_h2d->GetNbinsX(), values_h2d->GetNbinsY());
    }
    final_obj_2d[0] = values_h2d;
    final_obj_2d[1] = inter0_h2d;
    final_obj_2d[2] = inter1_h2d;
    final_obj_2d[3] = inter2_h2d;
    final_obj_2d[4] = inter3_h2d;
    final_obj_2d[5] = inter4_h2d;
    final_obj_2d[6] = inter5_h2d;
    final_obj_2d[7] = inter6_h2d;
    final_obj_2d[8] = inter7_h2d;
    final_obj_2d[9] = inter8_h2d;
    final_obj_2d[10] = inter9_h2d;
    if (verbose != 0) {
      printf("load_values finished!\n");
    }
    return reinterpret_cast<TObject **>(final_obj_2d);  // NOLINT
  }
  bool isZeroBased = (global_minpad == 0);
  int volt_pos = find(volt_vec.begin(), volt_vec.end(), selector) - volt_vec.begin();
  if (verbose > 0) {
    printf("Setting values for all pads and voltage %.2f\n", selector);
  }
  if (verbose > 1) {
    printf("maxpad %d minpad %d TH1F('values_h1d','values_h1d',%d,%f,%f)\n", maxpad[volt_pos], minpad[volt_pos],
           global_maxpad, 0.5, global_maxpad - 0.5);
  }
  auto *values_h1d = new TH1F("values_h1d", "values_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter0_h1d = new TH1F("inter0_h1d", "inter0_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter1_h1d = new TH1F("inter1_h1d", "inter1_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter2_h1d = new TH1F("inter2_h1d", "inter2_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter3_h1d = new TH1F("inter3_h1d", "inter3_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter4_h1d = new TH1F("inter4_h1d", "inter4_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter5_h1d = new TH1F("inter5_h1d", "inter5_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter6_h1d = new TH1F("inter6_h1d", "inter6_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter7_h1d = new TH1F("inter7_h1d", "inter7_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter8_h1d = new TH1F("inter8_h1d", "inter8_h1d", global_maxpad, 0.5, global_maxpad - 0.5);
  auto *inter9_h1d = new TH1F("inter9_h1d", "inter9_h1d", global_maxpad, 0.5, global_maxpad - 0.5);

  for (int i_pad = global_minpad; i_pad <= global_maxpad; ++i_pad) {
    if (isPadFilled[volt_pos][i_pad] == 0) {
      // printf("Skip bin %d at volt_pos %d\n",i_pad,volt_pos );
      continue;
    }
    int this_bin = isZeroBased ? i_pad + 1 : i_pad;
    if (verbose > 1) {
      printf("Setting bin %d value to %.2e +- %.2e\n", this_bin, values_h2d->GetBinContent(i_pad + 1, volt_pos + 1),
             values_h2d->GetBinError(i_pad + 1, volt_pos + 1));
      if (highestInterPad > -1) {
        printf("Inter-pad values: %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e\n",
               inter0_h2d->GetBinContent(i_pad + 1, volt_pos + 1), inter1_h2d->GetBinContent(i_pad + 1, volt_pos + 1),
               inter2_h2d->GetBinContent(i_pad + 1, volt_pos + 1), inter3_h2d->GetBinContent(i_pad + 1, volt_pos + 1),
               inter4_h2d->GetBinContent(i_pad + 1, volt_pos + 1), inter5_h2d->GetBinContent(i_pad + 1, volt_pos + 1),
               inter6_h2d->GetBinContent(i_pad + 1, volt_pos + 1), inter7_h2d->GetBinContent(i_pad + 1, volt_pos + 1),
               inter8_h2d->GetBinContent(i_pad + 1, volt_pos + 1), inter9_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
      }
    }
    values_h1d->SetBinContent(this_bin, values_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    values_h1d->SetBinError(this_bin, values_h2d->GetBinError(i_pad + 1, volt_pos + 1));
    inter0_h1d->SetBinContent(this_bin, inter0_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter1_h1d->SetBinContent(this_bin, inter1_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter2_h1d->SetBinContent(this_bin, inter2_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter3_h1d->SetBinContent(this_bin, inter3_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter4_h1d->SetBinContent(this_bin, inter4_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter5_h1d->SetBinContent(this_bin, inter5_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter6_h1d->SetBinContent(this_bin, inter6_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter7_h1d->SetBinContent(this_bin, inter7_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter8_h1d->SetBinContent(this_bin, inter8_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
    inter9_h1d->SetBinContent(this_bin, inter9_h2d->GetBinContent(i_pad + 1, volt_pos + 1));
  }
  if (verbose > 1) {
    printf("Histogram has %d xbins\n", values_h1d->GetNbinsX());
  }
  final_obj_1d[0] = values_h1d;
  final_obj_1d[1] = inter0_h1d;
  final_obj_1d[2] = inter1_h1d;
  final_obj_1d[3] = inter2_h1d;
  final_obj_1d[4] = inter3_h1d;
  final_obj_1d[5] = inter4_h1d;
  final_obj_1d[6] = inter5_h1d;
  final_obj_1d[7] = inter6_h1d;
  final_obj_1d[8] = inter7_h1d;
  final_obj_1d[9] = inter8_h1d;
  final_obj_1d[10] = inter9_h1d;
  if (verbose != 0) {
    printf("load_values finished!\n");
  }
  return reinterpret_cast<TObject **>(final_obj_1d);  // NOLINT
}
TObject *hex_values::load_value_average(double selector) {
  if (verbose != 0) {
    printf("Executing load_value_average...\n");
  }

  std::vector<std::string> fileList = split(inputFile, ',');

  bool is1d = singlePad || selector != AUTOPAD_SELECTOR;

  TH1F *h1d = nullptr;
  TH2F *h2d = nullptr;
  if (verbose > 1) {
    printf("Loading a %dD histogram!\n", is1d ? 1 : 2);
  }
  detailedPlotsDrawn = true;
  if (is1d) {
    h1d = dynamic_cast<TH1F *>(load_values(fileList.at(0), selector)[0]);
    h1d->Reset();
  } else {
    h2d = dynamic_cast<TH2F *>(load_values(fileList.at(0), selector)[0]);
    h2d->Reset();
  }
  int n_valid_files(0);
  int nbins_first_file(0);
  for (unsigned int i_file = 0; i_file < fileList.size(); ++i_file) {
    std::string thisInputFile = fileList.at(i_file);
    if (verbose != 0) {
      printf("Getting file %s!\n", thisInputFile.c_str());
    }
    TH1F *h1dtmp = nullptr;
    TH2F *h2dtmp = nullptr;
    detailedPlotsDrawn = false;
    if (is1d) {
      h1dtmp = dynamic_cast<TH1F *>(load_values(thisInputFile, selector)[0]);
      if (h1dtmp->Integral() == 0) {
        printf("No content in histogram corresponding to %s! Skip it!\n", thisInputFile.c_str());
        continue;
      }
      if (verbose > 1) {
        printf("Filling %d bin contents into graph\n", h1dtmp->GetNbinsX());
      }
      bool skipfile = false;
      for (int j_bin = 0; j_bin < h1dtmp->GetNbinsX(); ++j_bin) {
        if (n_valid_files == 0) {
          GR_CONTENTS[j_bin] = new TGraph(0);
          nbins_first_file = h1dtmp->GetNbinsX();
        }
        if (nbins_first_file != h1dtmp->GetNbinsX()) {
          skipfile = true;
          printf("Incompatible number of bins in histogram corresponding to %s! Skip it!\n", thisInputFile.c_str());
          break;
        }
        double cont = h1dtmp->GetBinContent(j_bin + 1);
        GR_CONTENTS[j_bin]->SetPoint(n_valid_files, n_valid_files, cont);
        SENSOR_VALS_FOR_AVERAGE[i_file][j_bin] = cont;
      }
      if (skipfile) {
        continue;
      }
      VALID_SENSOR_NAMES[n_valid_files] = get_file_name_no_ext(fileList.at(i_file), "_");
      ++n_valid_files;
      delete h1dtmp;
    } else {
      h2dtmp = dynamic_cast<TH2F *>(load_values(thisInputFile, selector)[0]);
      if (h2dtmp->Integral() == 0) {
        printf("No content in histogram corresponding to %s!\n", thisInputFile.c_str());
        continue;
      }
      for (int j_bin = 0; j_bin < h2dtmp->GetSize(); ++j_bin) {
        double oldcont = h2d->GetBinContent(j_bin + 1);
        double tmpcont = h2dtmp->GetBinContent(j_bin + 1);
        double newcont = (oldcont * n_valid_files + tmpcont) / (n_valid_files + 1);
        h2d->SetBinContent(j_bin + 1, newcont);
        if (n_valid_files == 0) {
          GR_CONTENTS[j_bin] = new TGraph(0);
        }
        GR_CONTENTS[j_bin]->SetPoint(n_valid_files, n_valid_files, h2dtmp->GetBinContent(j_bin + 1));
      }
      ++n_valid_files;
      delete h2dtmp;
    }
  }
  if (verbose > 1) {
    printf("Copy bin contents into average histogram\n");
  }
  if (is1d) {
    for (int j_bin = 0; j_bin < h1d->GetNbinsX(); ++j_bin) {
      double grmax = get_maximum(GR_CONTENTS[j_bin]);
      double grmin = get_minimum(GR_CONTENTS[j_bin]);
      double range = grmax - grmin;
      auto *h_errors = new TH1F(Form("h_errors"), Form("h_errors"), 100, grmin - 0.01 * range, grmax + 0.01 * range);
      for (int i_file = 0; i_file < n_valid_files; ++i_file) {
        h_errors->Fill(GR_CONTENTS[j_bin]->GetY()[i_file]);
      }
      // printf("Bin %d with content %f (%f), adding error
      // %f\n",j_bin+1,h1d->GetBinContent(j_bin+1),h_errors->GetMean(),h_errors->GetRMS());
      h1d->SetBinContent(j_bin + 1, h_errors->GetMean());
      h1d->SetBinError(j_bin + 1, h_errors->GetRMS());
      h_errors->Delete();
    }
  } else {
    for (int j_bin = 0; j_bin < h2d->GetSize(); ++j_bin) {
      double grmax = get_maximum(GR_CONTENTS[j_bin]);
      double grmin = get_minimum(GR_CONTENTS[j_bin]);
      double range = grmax - grmin;
      auto *h_errors = new TH1F(Form("h_errors"), Form("h_errors"), 100, grmin - 0.01 * range, grmax + 0.01 * range);
      for (int i_file = 0; i_file < n_valid_files; ++i_file) {
        h_errors->Fill(GR_CONTENTS[j_bin]->GetY()[i_file]);
      }
      // printf("Bin %d / %d with content %f (%f), adding error
      // %f\n",j_bin+1,h2d->GetSize(),h2d->GetBinContent(j_bin+1),h_errors->GetMean(),h_errors->GetRMS());
      h2d->SetBinContent(j_bin + 1, h_errors->GetMean());
      h2d->SetBinError(j_bin + 1, h_errors->GetRMS());
      h_errors->Delete();
    }
  }

  N_VALID_FILES = n_valid_files;

  if (verbose != 0) {
    printf("load_value_average finished!\n");
  }

  if (is1d) {
    return h1d;
  }
  { return h2d; }
}
double *hex_values::fit_pad_value_curve(TGraphErrors *gr_values, TSpline5 **s_spline, const std::string &fitFunc,
                                        int verbose) {
  auto *result = new double[2];
  TSpline5 *inter_spline;
  if (verbose > 1) {
    printf("Performing spline fit\n");
  }

  if (contains(fitFunc, "findvalue")) {
    double thisSelector = double_from_string(fitFunc);
    inter_spline = new TSpline5("gr_fill_std", gr_values);
    double minbinx = gr_values->GetX()[0];
    double maxbinx = gr_values->GetX()[gr_values->GetN() - 1];
    result[0] = find_first_x_above(inter_spline, thisSelector, minbinx, maxbinx);
    result[1] = 0;
  } else if (contains(fitFunc, "maxcurvature")) {
    inter_spline = new TSpline5("gr_fill_std", gr_values);

    int skip = 3;
    int neval = 0;
    int nbin = gr_values->GetN() - 1;
    double vbin = 0;
    std::vector<double> volts;
    std::vector<double> curv;
    double x, dx, knotX, knotY, B, C, D, E, F;
    for (int i = skip; i < nbin; i++) {
      inter_spline->GetCoeff(i, knotX, knotY, B, C, D, E, F);
      vbin = gr_values->GetX()[i + 1] - gr_values->GetX()[i];
      neval = int(vbin * 2);
      for (int j = 0; j < neval; j++) {
        dx = j * 0.5;
        x = knotX + dx;
        volts.push_back(x);
        curv.push_back(2 * C + 6 * D * dx + 12 * E * pow(dx, 2) + 20 * F * pow(dx, 3));
      }
    }
    auto itmin = std::distance(curv.begin(), std::min_element(curv.begin(), curv.end()));
    if (verbose > 1) {
      for (int j = 0; j < curv.size(); j++) {
        std::cout << volts.at(j) << "\t" << curv.at(j) << std::endl;
      }
      std::cout << volts.at(itmin) << "\t" << curv.at(itmin) << std::endl;
    }
    result[0] = volts.at(itmin);
    result[1] = 0.5;
  } else {
    printf("Error: operation %s not implemented!\n", fitFunc.c_str());
    inter_spline = new TSpline5("gr_fill_std", gr_values);
    result[0] = 0;
    result[1] = 0;
  }

  *s_spline = inter_spline;
  return result;
}
double *hex_values::fit_pad_value_curve(TGraphErrors *gr_values, TF1 **f_fitFunc, const std::string &fitFunc,
                                        int verbose) {
  int thisPadNumber = digits_from_string(gr_values->GetName()) + 1;

  auto *result = new double[2];
  double minY = get_minimum(gr_values);
  double maxY = get_maximum(gr_values);
  double minbinx = gr_values->GetX()[0];
  double maxbinx = gr_values->GetX()[gr_values->GetN() - 1];
  double almostminbinx = gr_values->GetX()[1];
  double almostmaxbinx = gr_values->GetX()[gr_values->GetN() - 2];
  double almostminbincont = gr_values->GetY()[1];
  double almostmaxbincont = gr_values->GetY()[gr_values->GetN() - 2];
  if (minbinx > maxbinx) {
    std::swap(minbinx, maxbinx);
    std::swap(almostminbinx, almostmaxbinx);
  }
  double fitrange = maxbinx - minbinx;
  std::string fit_options = verbose > 1 ? "0" : "Q0";

  std::vector<std::string> matches = split(fitFunc, ',');
  std::vector<double> rngs;
  for (unsigned int i = 1; i < matches.size(); i++) {
    rngs.push_back(std::atof(matches.at(i).c_str()));
  }

  if (verbose > 1) {
    std::cout << "Fit function: " << matches.at(0) << " with " << rngs.size() << " range parameters" << std::endl;
  }

  TF1 *f_fit;

  if (matches.at(0) == "linlin" || matches.at(0) == "linconst" || matches.at(0) == "constlin") {
    ranged_fit_func *fit_function = new ranged_fit_func(fit_function_linearintersection, rngs);
    f_fit = new TF1("f_volt_bias", fit_function, minbinx - 0.02 * fitrange, maxbinx + 0.02 * fitrange, 4);
    f_fit->SetParameters((almostmaxbinx + almostminbinx) / 2, (almostmaxbincont + almostminbincont) / 2,
                         (maxY - minY) / fitrange, 0);
    f_fit->SetParNames("Vdep", "intercept1", "slope1", "slope2");
    // make the one of the lines a constant by forcing its gradient to 0
    if (matches.at(0) == "constlin") {
      f_fit->FixParameter(2, 0);
    }
    if (matches.at(0) == "linconst") {
      f_fit->FixParameter(3, 0);
    }
    f_fit->SetParLimits(0, almostminbinx, almostmaxbinx);
    // if more than two ranges are given, use the center of the gap between the first two as a guess for vdep
    if (rngs.size() > 2) {
      f_fit->SetParameter(0, (rngs[1] + rngs[2]) / 2.0);
    }
    // do the fit and extract the depletion voltage
    gr_values->Fit(f_fit, fit_options.c_str());
    result[0] = f_fit->GetParameter(0);
    result[1] = f_fit->GetParError(0);

    double parLimits[2];
    f_fit->GetParLimits(0, parLimits[0], parLimits[1]);
    double parRange = parLimits[1] - parLimits[0];
    double diffUp = (parLimits[1] - result[0]) / parRange;
    double diffDown = (result[0] - parLimits[0]) / parRange;
    double MAX_DIFF_FIT_RANGE = 0.001;
    if (diffUp < MAX_DIFF_FIT_RANGE || diffDown < MAX_DIFF_FIT_RANGE) {
      printf("Warning: pad %d fit result is near parameter limit (%f < %f < %f). Check sanity!\n", thisPadNumber,
             parLimits[0], result[0], parLimits[1]);
    }

  } else if (matches.at(0) == "linear") {
    ranged_fit_func *fit_function = new ranged_fit_func(linear, rngs);
    f_fit = new TF1("f_volt_bias", fit_function, minbinx - 0.02 * fitrange, maxbinx + 0.02 * fitrange, 2);
    gr_values->Fit(f_fit, fit_options.c_str());
    double a = f_fit->GetParameter(0);
    double ae = f_fit->GetParError(0);
    double b = f_fit->GetParameter(1);
    double be = f_fit->GetParError(1);
    // a + b * x
    result[0] = -a / b;
    result[1] = sqrt(pow(-ae / b, 2) + pow(be * a / pow(b, 2), 2));

  } else {
    printf("Error: fit function not implemented!\n");
    f_fit = new TF1("not_implemented", "0", minbinx - 0.02 * fitrange, maxbinx + 0.02 * fitrange);
    result[0] = 0;
    result[1] = 0;
  }

  *f_fitFunc = f_fit;

  if (result[0] == -1 && result[1] == -1) {
    printf("Warning: fit was not performed! Check specified fit function!\n");
  }
  return result;
}
TObject *hex_values::load_derived_values(const std::string &thisInputFile) {
  if (verbose != 0) {
    printf("Executing load_derived_values...\n");
  }
  TH2F *h_fill = nullptr;
  detailedPlotsDrawn = true;
  h_fill = dynamic_cast<TH2F *>(load_values(thisInputFile, ALLPAD_SELECTOR)[0]);

  auto *h1dtmp = (TH1F *)h_fill->ProjectionX("h1dtmp");  // NOLINT
  int newnbin = h1dtmp->GetNbinsX();
  double newbinmin = h1dtmp->GetBinCenter(1) - h1dtmp->GetBinWidth(1) / 2;
  double newbinmax = h1dtmp->GetBinCenter(newnbin) + h1dtmp->GetBinWidth(newnbin) / 2;
  delete h1dtmp;

  auto *h1d = new TH1F("h1d", "h1d", newnbin, newbinmin, newbinmax);

  // now determine depletion voltage, i.e. plot 1/C^2 vs U
  TGraphErrors *gr_fill_std[MAX_CELL_NUMBER];
  TGraphErrors *gr_fill_trafo[MAX_CELL_NUMBER];
  double multiplicator = 1000.;  // scale transformation by this amount
  int nvolts_help = h_fill->GetNbinsY();
  int minpad_help = 0;
  int maxpad_help = h_fill->GetNbinsX() - 1;
  for (int ipad = minpad_help; ipad <= maxpad_help; ++ipad) {
    // printf("Now fitting pad %d\n", ipad );
    TF1 *f_volt_bias;
    TSpline5 *inter_spline;
    bool isFinal = (ipad == maxpad_help);
    gr_fill_std[ipad] = new TGraphErrors(0);
    gr_fill_std[ipad]->SetName(Form("gr_fill_std[%d]", ipad));
    gr_fill_trafo[ipad] = new TGraphErrors(0);
    gr_fill_trafo[ipad]->SetName(Form("gr_fill_trafo[%d]", ipad));
    double sumentries = 0;
    int npoint = 0;
    for (int ivolt = 0; ivolt < nvolts_help; ++ivolt) {
      double cont = h_fill->GetBinContent(ipad + 2, ivolt + 1);
      double err = h_fill->GetBinError(ipad + 2, ivolt + 1);
      if (cont != cont) {
        continue;
      }
      sumentries += cont;
      gr_fill_std[ipad]->SetPoint(npoint, volt_vec.at(ivolt), cont);
      gr_fill_std[ipad]->SetPointError(npoint, 0, err);
      double newcont = cont > 0 ? multiplicator / pow(cont, 2) : -1;
      double newerr = cont > 0 ? multiplicator * err * 2. / pow(cont, 3) : 0;
      gr_fill_trafo[ipad]->SetPoint(npoint, volt_vec.at(ivolt), newcont);
      gr_fill_trafo[ipad]->SetPointError(npoint, 0, newerr);
      ++npoint;
    }
    if (sumentries == 0) {
      continue;
    }

    // determine the value
    if (verbose > 1) {
      printf("Fitting CV curve for pad %d\n", ipad);
    }
    std::string operation;
    double *result;
    if (contains(fitFunc, "linlin") || contains(fitFunc, "linconst") || contains(fitFunc, "constlin") ||
        contains(fitFunc, "linear")) {
      operation = "TF1_operation";
      if (noTrafo) {
        result = fit_pad_value_curve(gr_fill_std[ipad], &f_volt_bias, fitFunc, verbose);
      } else {
        result = fit_pad_value_curve(gr_fill_trafo[ipad], &f_volt_bias, fitFunc, verbose);
      }
    } else if (contains(fitFunc, "findvalue") || contains(fitFunc, "maxcurvature")) {
      operation = "spline_operation";
      if (noTrafo) {
        result = fit_pad_value_curve(gr_fill_std[ipad], &inter_spline, fitFunc, verbose);
      } else {
        result = fit_pad_value_curve(gr_fill_trafo[ipad], &inter_spline, fitFunc, verbose);
      }
    }
    if (verbose > 1) {
      printf("Cell %d has fitted values %.2f +- %.2f\n", ipad, result[0], result[1]);
    }

    h1d->SetBinContent(ipad + 1, result[0]);
    h1d->SetBinError(ipad + 1, result[1]);

    if (detailedPlots) {
      if (verbose > 1) {
        printf("Now produce detailed depletion voltage plots\n");
      }
      // now plot it
      std::vector<std::string> matches = split(fitFunc, ',');
      std::vector<double> rngs;
      for (unsigned int i = 1; i < matches.size(); i++) {
        rngs.push_back(std::atof(matches.at(i).c_str()));
      }
      bool rangedfit = (!rngs.empty());

      std::string detailedSaveName = strip_extension(get_file_name_no_ext(thisInputFile));
      auto *c_chanbias =
          new TCanvas(Form("c_%s_chanbias[%d]", detailedSaveName.c_str(), ipad),
                      Form("c_%s_chanbias[%d]", detailedSaveName.c_str(), ipad), 800, 500, 750, int(750 / 1.5));
      auto *leftaxispad = new TPad("leftaxispad", "", 0., 0., 1., 1.);
      auto *rightaxispad = new TPad("rightaxispad", "", 0., 0., 1., 1.);
      rightaxispad->SetFillStyle(4000);  // will be transparent
      auto RightMargin = Float_t(noTrafo ? 0.05 : 0.13);
      Float_t TopMargin = 0.05;
      Float_t BottomMargin = 0.1;
      Float_t LeftMargin = 0.13;

      // draw leftaxispad
      leftaxispad->Draw();
      leftaxispad->cd();
      gPad->SetRightMargin(RightMargin);
      gPad->SetTopMargin(TopMargin);
      gPad->SetBottomMargin(BottomMargin);
      gPad->SetLeftMargin(LeftMargin);
      gPad->SetGridx();
      gr_fill_std[ipad]->GetHistogram()->GetXaxis()->SetTitle(
          Form("%s [%s]", selectorName.c_str(), selectorUnit.c_str()));
      gr_fill_std[ipad]->GetHistogram()->GetYaxis()->SetTitle(Form("%s [%s]", valueName.c_str(), valueUnit.c_str()));
      gr_fill_std[ipad]->GetHistogram()->GetYaxis()->SetTitleOffset(2);
      gr_fill_std[ipad]->SetTitle("");
      gr_fill_std[ipad]->SetMarkerStyle(20);
      gr_fill_std[ipad]->SetMarkerSize(0.8);
      gr_fill_std[ipad]->DrawClone("pa");
      gr_fill_std[ipad]->DrawClone("pesame");

      leftaxispad->Update();  // this will force the generation of the "stats" box
      leftaxispad->Modified();
      double xmin = gPad->GetUxmin();  // get value from this pad!
      double xmax = gPad->GetUxmax();  // get value from this pad!
      double ymin = gr_fill_std[ipad]->GetHistogram()->GetMinimum();
      double ymax = gr_fill_std[ipad]->GetHistogram()->GetMaximum();

      std::string resultLabel = "result";
      if (contains(fitFunc, "linlin") || contains(fitFunc, "linconst") || contains(fitFunc, "constlin") ||
          contains(fitFunc, "linear") || contains(fitFunc, "maxcurvature")) {
        resultLabel = "full depletion at";
      }
      if (contains(fitFunc, "findvalue")) {
        resultLabel = "voltage at " + double_to_string(double_from_string(fitFunc)) + " " + valueUnit + ":";
      }
      std::string line = Form("Pad %d: %s %.2f #pm %.2f V", ipad + 1, resultLabel.c_str(), result[0], result[1]);
      draw_TPave(line, LeftMargin, 0.97, LeftMargin, 0.97);

      if (!noTrafo) {
        // draw rightaxispad
        c_chanbias->cd();
        // compute the pad range with suitable margins
        ymin = gr_fill_trafo[ipad]->GetHistogram()->GetMinimum();
        ymax = gr_fill_trafo[ipad]->GetHistogram()->GetMaximum();
        // make consistent margins
        Double_t dy = (ymax - ymin) / (1 - TopMargin - BottomMargin);
        Double_t dx = (xmax - xmin) / (1 - RightMargin - LeftMargin);
        rightaxispad->Range(xmin - LeftMargin * dx, ymin - BottomMargin * dy, xmax + RightMargin * dx,
                            ymax + TopMargin * dy);
        rightaxispad->Draw();
        rightaxispad->cd();
        gr_fill_trafo[ipad]->SetTitle("");
        gr_fill_trafo[ipad]->SetMarkerStyle(20);
        gr_fill_trafo[ipad]->SetMarkerSize(0.8);
        gr_fill_trafo[ipad]->SetLineColor(kBlue);
        gr_fill_trafo[ipad]->SetMarkerColor(kBlue);
        // gr_fill_trafo[ipad]->DrawClone("pla");
        gr_fill_trafo[ipad]->DrawClone("Xpsames");
        rightaxispad->Update();

        // draw axis on the right side of the pad
        auto *rightaxis = new TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 110, "+L");
        rightaxis->SetTitle(Form("1/C^{2} [%.0f/pF^{2}]", multiplicator));
        rightaxis->SetTitleOffset(1.5);
        rightaxis->SetTitleSize(gr_fill_std[ipad]->GetHistogram()->GetYaxis()->GetTitleSize());
        rightaxis->SetLabelOffset(Float_t(gr_fill_std[ipad]->GetHistogram()->GetYaxis()->GetLabelOffset() * 1.5));
        rightaxis->SetLabelSize(gr_fill_std[ipad]->GetHistogram()->GetYaxis()->GetLabelSize());
        rightaxis->SetTitleFont(gr_fill_std[ipad]->GetHistogram()->GetYaxis()->GetTitleFont());
        rightaxis->SetLabelFont(gr_fill_std[ipad]->GetHistogram()->GetYaxis()->GetLabelFont());
        rightaxis->SetLabelColor(kBlue);
        rightaxis->SetTitleColor(kBlue);
        rightaxis->Draw();
      }

      // draw fit function
      if (noTrafo) {
        leftaxispad->cd();
      } else {
        rightaxispad->cd();
      }

      Double_t x = result[0];
      Double_t y = 0;

      if (operation == "TF1_operation") {
        f_volt_bias->SetLineWidth(1);
        // if it was ranged draw the whole line dotted
        if (rangedfit) {
          f_volt_bias->SetLineStyle(3);
        }
        f_volt_bias->DrawClone("same");
        y = f_volt_bias->Eval(x);

        // draw solid lines showing where the fit ranges were
        if (rangedfit) {
          // draw segments in full
          f_volt_bias->SetLineStyle(1);
          for (unsigned int i = 0; i < rngs.size(); i += 2) {
            if (i < (rngs.size() - 1)) {
              f_volt_bias->SetRange(rngs.at(i), rngs.at(i + 1));  // NOLINT
              f_volt_bias->DrawClone("same");
            }
            if (i == (rngs.size() - 1)) {
              f_volt_bias->SetRange(rngs.at(i), 1000);
              f_volt_bias->DrawClone("same");
            }
          }
        }
      } else if (operation == "spline_operation") {
        inter_spline->SetLineColor(kRed);
        inter_spline->Draw("same");
        if (x == x) {
          y = inter_spline->Eval(x);
        } else {
          y = std::numeric_limits<double>::quiet_NaN();
        }
      }

      if (x > xmin && x < xmax) {
        if (contains(fitFunc, "findvalue")) {
          double thisSelector = double_from_string(fitFunc);
          auto *downArrow = new TArrow(x, thisSelector, x, ymin, 0.03, ">");
          downArrow->SetLineWidth(1);
          downArrow->SetLineColor(kRed);
          downArrow->Draw();
          auto *rightArrow = new TArrow(xmin, thisSelector, x, thisSelector, 0.03, ">");
          rightArrow->SetLineWidth(1);
          rightArrow->SetLineColor(kRed);
          rightArrow->Draw();
        } else {
          double yTopSpace = ymax - y;
          auto *resultArrow = new TArrow(x, y + 0.9 * yTopSpace, x, y + 0.1 * yTopSpace, 0.03, ">");
          resultArrow->SetLineWidth(1);
          resultArrow->SetLineColor(kRed);
          resultArrow->Draw();
        }
        // TLine *resultLine = new TLine(x, ymin, x, ymax);
        // resultLine->SetLineWidth(1);
        // resultLine->SetLineColor(kRed);
        // resultLine->Draw();
      }

      // plot it
      plot_this_pad(c_chanbias, detailedSaveName, isFinal, "depletion_details", ipad + 1);
    }
  }

  if (verbose != 0) {
    printf("load_derived_values finished!\n");
  }
  return h1d;
}

TObject *hex_values::load_derived_value_average() {
  if (verbose != 0) {
    printf("Executing load_derived_value_average...\n");
  }

  std::vector<std::string> fileList = split(inputFile, ',');

  TH1F *h_tmp = nullptr;
  TH1F *h_mean = nullptr;
  int n_valid_files = 0;
  int nbins_first_file = 0;
  for (unsigned int ifile = 0; ifile < fileList.size(); ++ifile) {
    h_tmp = dynamic_cast<TH1F *>(load_derived_values(fileList.at(ifile)));
    if (nbins_first_file == 0) {
      h_mean = dynamic_cast<TH1F *>(h_tmp->Clone());
      nbins_first_file = h_mean->GetNbinsX();
      for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin) {
        GR_CONTENTS[ibin] = new TGraph(0);
        double newval = h_tmp->GetBinContent(ibin + 1);
        GR_CONTENTS[ibin]->SetPoint(n_valid_files, n_valid_files, newval);
      }

    } else {
      if (nbins_first_file != h_mean->GetNbinsX()) {
        printf("Incompatible number of bins in histogram corresponding to %s! Skip it!\n", fileList.at(ifile).c_str());
        continue;
      }
      for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin) {
        double oldcont = h_mean->GetBinContent(ibin + 1);
        double newval = h_tmp->GetBinContent(ibin + 1);
        double newcont = (oldcont * ifile + newval) / (ifile + 1);
        h_mean->SetBinContent(ibin + 1, newcont);
        GR_CONTENTS[ibin]->SetPoint(n_valid_files, n_valid_files, newval);
      }
    }
    ++n_valid_files;
  }
  N_VALID_FILES = n_valid_files;
  if (verbose != 0) {
    printf("load_derived_value_average finished!\n");
  }
  return h_mean;
}
TObject **hex_values::load_data_histo(bool is1D) {
  if (verbose != 0) {
    printf("Executing load_data_histo...\n");
  }

  effectiveSelector = selector;

  auto **final_obj_1d = new TH1F *[7];
  auto **final_obj_2d = new TH2F *[7];

  if (is1D) {
    if (verbose != 0) {
      printf("Loading 1D histogram!\n");
    }
    if (valueOption == "DEP") {
      if (!isAverageValues) {
        final_obj_1d[0] = dynamic_cast<TH1F *>(load_derived_values(inputFile));
      } else {
        final_obj_1d[0] = dynamic_cast<TH1F *>(load_derived_value_average());
      }
    } else {
      if (!isAverageValues) {
        final_obj_1d = reinterpret_cast<TH1F **>(load_values(inputFile, selector));  // NOLINT
      } else {
        final_obj_1d[0] = dynamic_cast<TH1F *>(load_value_average(selector));
      }
    }
  } else {
    if (verbose != 0) {
      printf("Loading 2D histogram!\n");
    }
    if (!isAverageValues) {
      final_obj_2d = reinterpret_cast<TH2F **>(load_values(inputFile, ALLPAD_SELECTOR));  // NOLINT
    } else {
      final_obj_2d[0] = dynamic_cast<TH2F *>(load_value_average(ALLPAD_SELECTOR));
    }
  }

  if (verbose != 0) {
    printf("load_data_histo finished!\n");
  }
  if (is1D) {
    return reinterpret_cast<TObject **>(final_obj_1d);  // NOLINT
  }
  {
    return reinterpret_cast<TObject **>(final_obj_2d);  // NOLINT
  }
}
void hex_values::plot_this_pad(TCanvas *singleCanv, const std::string &saveName, bool isFinal,
                               const std::string &description, int startIndex, int endIndex) {
  if (cumulativePlotsMap.find(description) == cumulativePlotsMap.end()) {
    cumulativePlotsMap[description] = 0;
  }
  if (currentCanvIdxMap.find(description) == currentCanvIdxMap.end()) {
    currentCanvIdxMap[description] = 0;
  }
  if (detailedCanvMap.find(description) == detailedCanvMap.end()) {
    for (size_t i = 0; i < MAX_DETAILED_PLOT_NUMBER; i++) {
      TCanvas *c_dummy = nullptr;
      detailedCanvMap[description].push_back(c_dummy);
    }
  }
  if (currentCanvIdxMap[description] > MAX_DETAILED_PLOT_NUMBER) {
    printf("Warning: increase maximum number of detailed plot canvasses!\n");
  }
  int npadx = 4;
  int npady = 4;
  int npad = npadx * npady;
  int currentpad = cumulativePlotsMap[description] - currentCanvIdxMap[description] * npad + 1;
  if (currentpad == 1) {
    currentCanvStartIdxMap[description] = startIndex;
    detailedCanvMap[description].at(currentCanvIdxMap[description]) =
        new TCanvas(Form("c_%s_%s[%d-%d]", saveName.c_str(), description.c_str(),
                         currentCanvIdxMap[description] * npad + 1, (currentCanvIdxMap[description] + 1) * npad),
                    Form("c_%s_%s[%d-%d]", saveName.c_str(), description.c_str(),
                         currentCanvIdxMap[description] * npad + 1, (currentCanvIdxMap[description] + 1) * npad),
                    0, 0, 1200, 1000);
    detailedCanvMap[description].at(currentCanvIdxMap[description])->Divide(npadx, npady);
  }

  detailedCanvMap[description].at(currentCanvIdxMap[description])->cd(currentpad);
  gPad->SetRightMargin(0);
  gPad->SetLeftMargin(0);
  gPad->SetTopMargin(0);
  gPad->SetBottomMargin(0);

  singleCanv->DrawClonePad();

  ++cumulativePlotsMap[description];
  if (currentpad == npad || isFinal) {
    std::string fullSaveName = Form(
        "%s_%s_", std::string(OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + saveName).c_str(), description.c_str());
    if (endIndex == MAX_INTEGER) {
      endIndex = startIndex;
    }
    if (startIndex == MAX_INTEGER) {
      fullSaveName += Form("%d_to_%d", currentCanvIdxMap[description] * npad + 1,
                           currentCanvIdxMap[description] * npad + currentpad);
    } else {
      fullSaveName += int_to_string(currentCanvStartIdxMap[description]) + "_to_" + int_to_string(endIndex);
    }
    fullSaveName += ".pdf";
    detailedCanvMap[description].at(currentCanvIdxMap[description])->Print(fullSaveName.c_str());
    ++currentCanvIdxMap[description];
  }
  if (isFinal) {
    cumulativePlotsMap[description] = 0;
    currentCanvIdxMap[description] = 0;
  }
}
void hex_values::plot_detailed_plots(TH2F *h_fill, const std::string &fileName) {
  if (detailedPlotsDrawn) {
    return;
  }
  if (verbose > 1) {
    printf("Executing plot_detailed_plots\n");
  }
  detailedPlotsDrawn = true;
  TGraphErrors *gr_fill[MAX_CELL_NUMBER];
  int nvolts_help = h_fill->GetNbinsY();
  int minpad_help = 0;
  int maxpad_help = h_fill->GetNbinsX() - 1;
  const int MAX_IN_OVERLAY = 16;

  std::string detailedSaveName = strip_extension(get_file_name_no_ext(fileName));
  TCanvas *c_chanbias_overlay[MAX_DETAILED_PLOT_NUMBER] = {nullptr};
  auto *c_chanbias_all =
      new TCanvas(Form("c_%s_chanbias_all", detailedSaveName.c_str()),
                  Form("c_%s_chanbias_all", detailedSaveName.c_str()), 800, 500, 750, int(750 / 1.5));

  double xBinArray[MAX_SELECTOR_NUMBER];
  double lowEdge = volt_vec.at(0) - (volt_vec.at(1) - volt_vec.at(0)) / 2;
  xBinArray[0] = lowEdge;
  for (int i = 0; i < nvolts_help - 1; i++) {
    xBinArray[i + 1] = (volt_vec.at(i) + volt_vec.at(i + 1)) / 2;  // NOLINT
  }
  double highEdge = volt_vec.back() + (volt_vec.back() - volt_vec.at(volt_vec.size() - 2)) / 2;
  xBinArray[nvolts_help] = highEdge;
  if (xBinArray[nvolts_help] < xBinArray[0]) {
    std::vector<double> tmpVolt;
    for (int i = nvolts_help; i >= 0; --i) {
      tmpVolt.push_back(xBinArray[i]);
    }
    for (int i = 0; i <= nvolts_help; ++i) {
      xBinArray[i] = tmpVolt.at(i);
    }
  }

  const bool USE_OVERLAY_LOG_Y = true;
  const int N_Y_BINS = 100;
  double yBinArray[N_Y_BINS + 1];
  double maxY = (USE_OVERLAY_LOG_Y) ? maxNonZeroValue * 2 : h_fill->GetMaximum();
  double minY = (USE_OVERLAY_LOG_Y) ? minNonZeroValue / 2 : h_fill->GetMinimum();
  if (zoomMin != zoomMax) {
    maxY = zoomMax;
    minY = zoomMin;
  }
  double range = maxY - minY;
  maxNonZeroValue += range * 0.05;
  if (USE_OVERLAY_LOG_Y) {
    maxY = TMath::Log10(maxY);
    minY = TMath::Log10(minY);
  }
  range = maxY - minY;
  for (size_t i = 0; i < N_Y_BINS; i++) {
    yBinArray[i] = minY + i * range / (N_Y_BINS);
  }
  yBinArray[N_Y_BINS] = maxY;

  std::string heatName = Form(";%s [%s];%s [%s];Number of pads", selectorName.c_str(), selectorUnit.c_str(),
                              valueName.c_str(), valueUnit.c_str());
  auto *h_heat = new TH2F("h_heat", heatName.c_str(), nvolts_help, xBinArray, N_Y_BINS, yBinArray);
  if (USE_OVERLAY_LOG_Y) {
    make_log_bins(h_heat, "y");
  }

  int nPad = 0;
  std::string allPadLine;
  Float_t RightMargin = 0.05;
  Float_t TopMargin = 0.05;
  Float_t BottomMargin = 0.1;
  Float_t LeftMargin = 0.11;
  std::vector<int> overlayIndices;
  for (int ipad = minpad_help; ipad <= maxpad_help; ++ipad) {
    bool isFinal = (ipad == maxpad_help);
    gr_fill[ipad] = new TGraphErrors(0);
    double sumentries = 0;
    int npoint = 0;
    for (int ivolt = 0; ivolt < nvolts_help; ++ivolt) {
      double cont = h_fill->GetBinContent(ipad + 2, ivolt + 1);
      double err = h_fill->GetBinError(ipad + 2, ivolt + 1);
      if (cont != cont) {
        continue;
      }
      sumentries += cont;
      gr_fill[ipad]->SetName(Form("gr_fill[%d]", ipad));
      gr_fill[ipad]->SetPoint(npoint, volt_vec.at(ivolt), cont);
      gr_fill[ipad]->SetPointError(npoint, 0, err);
      ++npoint;
    }
    if (sumentries == 0) {
      continue;
    }

    gr_fill[ipad]->SetTitle("");
    gr_fill[ipad]->SetMarkerStyle(20);
    gr_fill[ipad]->SetMarkerSize(0.8);
    gr_fill[ipad]->GetHistogram()->GetXaxis()->SetTitle(Form("%s [%s]", selectorName.c_str(), selectorUnit.c_str()));
    gr_fill[ipad]->GetHistogram()->GetYaxis()->SetTitle(Form("%s [%s]", valueName.c_str(), valueUnit.c_str()));
    gr_fill[ipad]->GetHistogram()->GetYaxis()->SetTitleOffset(1.5);
    gr_fill[ipad]->GetHistogram()->GetYaxis()->SetRangeUser(get_minimum(gr_fill[ipad], true),
                                                            get_maximum(gr_fill[ipad], true));
    // gr_fill[ipad]->SetMarkerColor(get_color_line_fill_style(nPad)[0]);
    // gr_fill[ipad]->SetLineColor(get_color_line_fill_style(nPad)[0]);
    // gr_fill[ipad]->SetLineStyle(get_color_line_fill_style(nPad)[1]);

    // Single channel plots
    auto *c_chanbias =
        new TCanvas(Form("c_%s_chanbias[%d]", detailedSaveName.c_str(), ipad),
                    Form("c_%s_chanbias[%d]", detailedSaveName.c_str(), ipad), 800, 500, 750, int(750 / 1.5));
    c_chanbias->cd();
    gPad->SetRightMargin(RightMargin);
    gPad->SetTopMargin(TopMargin);
    gPad->SetBottomMargin(BottomMargin);
    gPad->SetLeftMargin(LeftMargin);
    gPad->SetGridx();
    gr_fill[ipad]->DrawClone("pla");
    std::string line = Form("Pad %d", ipad + 1);
    draw_TPave(line, LeftMargin, 0.97, LeftMargin, 0.97);
    plot_this_pad(c_chanbias, detailedSaveName, isFinal, "details", ipad + 1);

    // overlaid channel plots
    auto overPlotIdx = int(nPad / MAX_IN_OVERLAY);
    // gr_fill[ipad]->SetLineColor(get_color_line_fill_style(nPad)[0]);
    // gr_fill[ipad]->SetLineStyle(get_color_line_fill_style(nPad)[1]);
    if (nPad % MAX_IN_OVERLAY == 0) {
      c_chanbias_overlay[overPlotIdx] = new TCanvas(
          Form("c_%s_chanbias_all[%d]", detailedSaveName.c_str(), overPlotIdx),
          Form("c_%s_chanbias_all[%d]", detailedSaveName.c_str(), overPlotIdx), 800, 500, 750, int(750 / 1.5));
      c_chanbias_overlay[overPlotIdx]->cd();
      gPad->SetRightMargin(RightMargin);
      gPad->SetTopMargin(TopMargin);
      gPad->SetBottomMargin(BottomMargin);
      gPad->SetLeftMargin(LeftMargin);
      gPad->SetGridx();
      gr_fill[ipad]->Draw("la");
    } else {
      c_chanbias_overlay[overPlotIdx]->cd();
      gr_fill[ipad]->Draw("lsame");
    }
    overlayIndices.push_back(ipad);
    bool firstInOverlayPlot = (nPad % MAX_IN_OVERLAY == MAX_IN_OVERLAY - 1);
    bool printOverlay = (nPad != 0 && firstInOverlayPlot);
    if (printOverlay || isFinal) {
      double *ranges = get_plot_ranges(gr_fill, overlayIndices);
      // printf("Overlay plot %d set range %.2e - %.2e\n", overPlotIdx, ranges[0], ranges[1] );
      gr_fill[overlayIndices.at(0)]->GetHistogram()->GetYaxis()->SetRangeUser(ranges[0], ranges[1]);
      line = Form("Pads %d - %d", overlayIndices.at(0) + 1, ipad + 1);
      draw_TPave(line, LeftMargin, 0.97, LeftMargin, 0.97);
      plot_this_pad(c_chanbias_overlay[overPlotIdx], detailedSaveName, isFinal, "details_overlay",
                    overlayIndices.at(0) + 1, ipad + 1);
      overlayIndices.clear();
    }

    // all channel plots
    c_chanbias_all->cd();
    if (nPad == 0) {
      gPad->SetRightMargin(RightMargin);
      gPad->SetTopMargin(TopMargin);
      gPad->SetBottomMargin(BottomMargin);
      gPad->SetLeftMargin(LeftMargin);
      gPad->SetGridx();
      gPad->SetGridy();
      if (USE_OVERLAY_LOG_Y) {
        gPad->SetLogy();
        gr_fill[ipad]->SetMinimum(minNonZeroValue / 2);
        gr_fill[ipad]->SetMaximum(maxNonZeroValue * 2);
      }
      gr_fill[ipad]->Draw("la");
    } else {
      gr_fill[ipad]->Draw("lsame");
    }
    if (isFinal) {
      double *ranges = get_plot_ranges(gr_fill, ipad);
      if (ranges[0] * ranges[1] < 0) {
        gPad->SetLogy(0);
      }
      for (int i = minpad_help; i <= maxpad_help; ++i) {
        if (!USE_OVERLAY_LOG_Y) {
          gr_fill[i]->SetMaximum(ranges[1]);
          gr_fill[i]->SetMinimum(ranges[0]);
        }
        if (zoomMin != zoomMax) {
          gr_fill[i]->SetMaximum(zoomMax);
          gr_fill[i]->SetMinimum(zoomMin);
        }
      }
      allPadLine = Form("All pads (%d - %d)", 1, ipad + 1);
      draw_TPave(allPadLine, LeftMargin, 0.97, LeftMargin, 0.97);
      c_chanbias_all->Print(Form("%s%s_details_all.pdf", (OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/")).c_str(),
                                 (detailedSaveName).c_str()));
    }
    // heat map
    for (int i = 0; i < gr_fill[ipad]->GetN(); i++) {
      double x = gr_fill[ipad]->GetX()[i];
      double y = gr_fill[ipad]->GetY()[i];
      h_heat->Fill(x, y);
    }

    ++nPad;
  }

  auto *c_heat = new TCanvas("c_heat", "c_heat", 800, 500, 750, int(750 / 1.5));
  c_heat->cd();
  gPad->SetTopMargin(TopMargin);
  gPad->SetLeftMargin(LeftMargin);
  gPad->SetRightMargin(0.15);
  gPad->SetLogz();
  gPad->SetGridy();
  if (USE_OVERLAY_LOG_Y) {
    gPad->SetLogy();
  }
  h_heat->SetMinimum(1);
  h_heat->GetYaxis()->SetTitleOffset(1.3);
  h_heat->Draw("colz");
  draw_bin_grid(h_heat, "x", 1, 0, 3);
  h_heat->Draw("colzaxissame");
  draw_TPave(allPadLine, LeftMargin, 0.97, LeftMargin, 0.97);
  c_heat->Print(Form("%s_details_all_heat.pdf",
                     std::string(OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + detailedSaveName).c_str()));

  if (verbose > 1) {
    printf("plot_detailed_plots finished\n");
  }
}
